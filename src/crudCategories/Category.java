package crudCategories;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class Category {
	//Attributes
	private int IdCategory;
	private String name;
	private Connection conn = null;
	private Statement stmt = null;

	//Constructor
	Category()  {
		try {
			//Connection
			this.conn = DriverManager.getConnection("jdbc:mysql://localhost/ecommerce?user=root&password=");

			//Create Statement
			this.stmt =  conn.createStatement();

		}catch(Exception ex) {
			System.out.println(ex.getMessage());
		}
	}



	//Methods
	public int getIdCategory() {
		return IdCategory;
	}

	public void setIdCategory(int idCategory) {
		IdCategory = idCategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	//CRUD
	public int CreateCategory(String newCategory) {

		int newid=0;

		try {
			String sql = "INSERT INTO categories (name) VALUES ('" + newCategory + "')";
			System.out.println(sql);

			int rowseffected = this.stmt.executeUpdate(sql);

			if (rowseffected  > 0) {
				sql = "SELECT MAX(idCategory) AS id FROM categories";

				//Execute query
				ResultSet rs  = this.stmt.executeQuery(sql);
				if (rs.next()) {
					newid = rs.getInt("id");
				}else {
					throw new Exception("Error to INSERT data");
				}
			}

			return newid;

		}catch(Exception ex){
			System.out.println(ex.getMessage());
			return 0;
		}
	}

	public ArrayList<Category> ReadCategories() {

		try {

			//SQL
			String sql = "Select * from categories";
			System.out.println(sql);

			//Execute query
			ResultSet rs  = this.stmt.executeQuery(sql);

			ArrayList<Category> categories = new ArrayList<Category>();

			while (rs.next()) {
				Category item = new Category();
				item.setIdCategory(rs.getInt("IdCategory"));
				item.setName(rs.getString("name"));

				categories.add(item);
			}

			return categories;

		}catch (Exception ex) {
			System.out.println(ex.getMessage());
			return null;
		}

	}

	public boolean UpdateCategory(int idCat , String categorytoUpdate) {
		try {
			String sql = "UPDATE categories SET name = '" + categorytoUpdate + "' WHERE idCategory ='" + idCat + "'";
			System.out.println(sql);
			
			int rowseffected = this.stmt.executeUpdate(sql);

			if (rowseffected > 0)
				return true;
			else
				throw new Exception("Error to UPDATE data");

		}catch(Exception ex){
			System.out.println(ex.getMessage());
			return false;
		}
	}

	public boolean DeleteCategoryByID(int idCat) {
		try {
			String sql = "DELETE from categories  where idcategory  ='" + idCat + "'";
			System.out.println(sql);
			
			int rowseffected = this.stmt.executeUpdate(sql);

			if (rowseffected > 0)
				return true;
			else
				throw new Exception("Error to DELETE data");

		}catch(Exception ex){
			System.out.println(ex.getMessage());
			return false;
		}
	}



}
