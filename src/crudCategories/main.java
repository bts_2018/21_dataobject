package crudCategories;

import java.util.ArrayList;

public class main {

	public static void main(String[] args) {
		 
		Category x = new Category();
		
		//CREATE
		int newID = x.CreateCategory("Bikes");
	
		
		//UPDATE
		x.UpdateCategory(newID, "Bikessssssss");
		
		//READ
		ArrayList<Category> items = x.ReadCategories();
		
		for (Category item :items) {
			System.out.println(">>>" + item.getName());
		}
		
		//DELETE
		x.DeleteCategoryByID(newID);
		
	}

}
